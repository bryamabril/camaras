import { Component } from '@angular/core';
import { VideoPage } from '../video/video';
import { SmsPage } from '../sms/sms';
import { MapasPage } from '../mapas/mapas';
import {AngularFireAuth} from 'angularfire2/auth';
import { NavController, ToastController, NavParams,IonicPage } from 'ionic-angular';

@IonicPage()

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  constructor(public navCtrl: NavController, private toast: ToastController,private autentica:AngularFireAuth, public navParams: NavParams) {
     
  }

  
  ionViewDidLoad() {
    this.autentica.authState.subscribe(data => {
    	this.toast.create({
        position: "top",
    		message: 'Bienvenido a ES3',
    		duration: 3000
    	}).present();
    });
    
  }
  videoPage = VideoPage;
  smsPage = SmsPage;
  mapasPage = MapasPage;
  
}
