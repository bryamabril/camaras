import { Component } from '@angular/core';
import { IonicPage, NavController,ToastController} from 'ionic-angular';
import { ContactProvider,ContactList } from '../../providers/contact/contact';
import { SMS } from '@ionic-native/sms';


@IonicPage()
@Component({
  selector: 'page-video',
  templateUrl: 'video.html',
})


export class VideoPage {
  contacts: ContactList[];
  GroupKey:any;
  ApiKey:any;
  MonitorId:any;
  Camara1:any;
  Camara2:any;
  Camara3:any;
  Camara4:any;
 
 public condicion:boolean;//condicion 

  constructor(public navCtrl: NavController, private contactProvider: ContactProvider, public toastCtrl: ToastController,private smsVar: SMS) {
   
    this.contactProvider.getAll()
    .then((result) => {
      this.contacts = result;
      
      let answer = this.contacts.map(x=> {
      let {name2, name3, name4} = x.contact;
      let obj = {name2,name3,name4}
      return obj
    })
    
    this.GroupKey= answer.map(({ name2}) => name2);
    this.ApiKey= answer.map(({ name4}) => name4);
    this.MonitorId = answer.map(({ name3}) => name3);
    console.log(answer)
    this.Camara1=this.ApiKey[0]+"/monitor"+this.GroupKey[0]+"/"+this.MonitorId[0];
    this.Camara2=this.ApiKey[1]+"/monitor"+this.GroupKey[1]+"/"+this.MonitorId[1];
    this.Camara3=this.ApiKey[2]+"/monitor"+this.GroupKey[2]+"/"+this.MonitorId[2];
    this.Camara4=this.ApiKey[3]+"/monitor"+this.GroupKey[3]+"/"+this.MonitorId[3];
    
    });
    
    
  }

  sendSMS(){
    if (this.condicion == true){
    var options={
          replaceLineBreaks: false, // true to replace \n by a new line, false by default
          android: {
               intent: 'INTENT'  // Opens Default sms app
             // intent: '' // Sends sms without opening default sms app
            }
    }
    this.smsVar.send('+593995049854', 'Activar',options)
      .then(()=>{
        alert("success");
      },()=>{
      alert("failed");
      });
    }
  }
  
}
 
