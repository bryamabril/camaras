import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Platform } from 'ionic-angular';
import { AndroidPermissions } from '@ionic-native/android-permissions';


declare var SMS:any;

@IonicPage()
@Component({
  selector: 'page-sms',
  templateUrl: 'sms.html',
})

export class SmsPage {
  
  messages:any=[];

  constructor(public navCtrl: NavController, public navParams: NavParams,public platform:Platform,
    public androidPermissions: AndroidPermissions) {
      this.checkPermission()
  }
  checkPermission()
  {
  this.androidPermissions.checkPermission
  (this.androidPermissions.PERMISSION.READ_SMS).then(
  success => {
            
  //if permission granted
  this.ReadSMSList();
  },
  err =>{
          
  this.androidPermissions.requestPermission
  (this.androidPermissions.PERMISSION.READ_SMS).
  then(success=>{
  this.ReadSMSList();
  },
  err=>{
  alert("cancelled")
  });
  });
        
  this.androidPermissions.requestPermissions
  ([this.androidPermissions.PERMISSION.READ_SMS]);
        
  }
  ReadSMSList()
  {
      
  this.platform.ready().then((readySource) => {
      
    let filter = {
      box : 'inbox', // 'inbox' (default), 'sent', 'draft'
      indexFrom : 0, // start from index 0
      maxCount : 5, // count of SMS to return each time
      read : 1,// 0 para poner que no se vean los leidos 
      address : '+2427',
    }; 
    
  if(SMS) SMS.listSMS(filter, (ListSms)=>{               
  this.messages=ListSms
  },
      
  Error=>{
  alert(JSON.stringify(Error))
  });
           
  });
  }
 
}