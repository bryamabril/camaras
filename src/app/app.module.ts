import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';






//storage
import { IonicStorageModule } from '@ionic/storage';
import { DatePipe } from '@angular/common';
import { ContactProvider } from '../providers/contact/contact';
// sms mostrar
import {FirstCapsPipe} from '../pipes/first-caps/first-caps';
import {AndroidPermissions} from '@ionic-native/android-permissions'

// sms enviar 
import {SMS} from '@ionic-native/sms';

//Paginas
import { HomePage } from '../pages/home/home';
import { VideoPage } from '../pages/video/video';
import { SmsPage } from '../pages/sms/sms';
import { MapasPage } from '../pages/mapas/mapas';
// SWIPE TABS
import { SuperTabsModule } from 'ionic2-super-tabs';

//MAPAS
import { GoogleMapsProvider } from '../providers/google-maps/google-maps';
import { Geolocation } from '@ionic-native/geolocation'



@NgModule({
  declarations: [
    HomePage,
   // MyApp,
    VideoPage,
    SmsPage,
    FirstCapsPipe,
    MapasPage
  
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(HomePage),
      //storage
    IonicStorageModule.forRoot(),
    //TABS
    SuperTabsModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    HomePage,
  //  MyApp,
    VideoPage,
    SmsPage,
    MapasPage

  ],
  providers: [
    StatusBar,
    SplashScreen,
    //storage
    DatePipe,
    ContactProvider,
    //mostrar sms
    AndroidPermissions,
    //enviar sms
    SMS,
    //Mapas
    GoogleMapsProvider,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ContactProvider
  ]
})
export class AppModule {}
